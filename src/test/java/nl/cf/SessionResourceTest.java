package nl.cf;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import nl.cf.service.SessionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class SessionResourceTest {

    @InjectMock
    private SessionService service;

    @Test
    void testSessionsEndpoint() {
        Mockito.when(service.getSessions()).thenReturn(List.of());
        given()
          .when().get("/sessions")
          .then()
             .statusCode(200)
             .body(is("[]"));
    }

}