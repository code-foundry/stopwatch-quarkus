package nl.cf.service;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import nl.cf.service.domain.Session;

import java.util.UUID;

@ApplicationScoped
class SessionRepository implements PanacheRepositoryBase<Session, UUID> {
}
