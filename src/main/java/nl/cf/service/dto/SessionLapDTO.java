package nl.cf.service.dto;

public record SessionLapDTO(Long lap){}
