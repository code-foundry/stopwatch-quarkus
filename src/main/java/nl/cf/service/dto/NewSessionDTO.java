package nl.cf.service.dto;

public record NewSessionDTO(String title) {}
