package nl.cf.service.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public record SessionDTO(
        UUID id,
        String title,
        LocalDateTime startedAt,
        Long resumptionTime,
        boolean finished,
        List<Long> laps
) {}