package nl.cf.service.dto;

import java.time.LocalDateTime;
import java.util.List;

public record PartialSessionDTO(
        String title,
        LocalDateTime startedAt,
        Long resumptionTime,
        Boolean finished, // may be null, hence not primitive
        List<Long> laps
) {}