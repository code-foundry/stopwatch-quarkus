package nl.cf.service.domain;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column
    private String title;

    @Column
    private LocalDateTime startedAt = null;

    @Column
    private Long resumptionTime = null;

    @Column
    private boolean finished = false;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "lap", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "laptime")
    private List<Long> laps;

    public Session() {
        // required no-args constructor
    }

    public Session(final String title) {
        this.title = title;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(final LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public Long getResumptionTime() {
        return resumptionTime;
    }

    public void setResumptionTime(final Long resumptionTime) {
        this.resumptionTime = resumptionTime;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(final boolean finished) {
        this.finished = finished;
    }

    public List<Long> getLaps() {
        return laps;
    }

    public void setLaps(final List<Long> laps) {
        this.laps = laps;
    }
}
