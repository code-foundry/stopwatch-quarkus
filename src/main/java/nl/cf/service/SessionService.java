package nl.cf.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import nl.cf.service.domain.Session;
import nl.cf.service.dto.NewSessionDTO;
import nl.cf.service.dto.PartialSessionDTO;
import nl.cf.service.dto.SessionDTO;

import java.util.List;
import java.util.UUID;

@Transactional
@ApplicationScoped
public class SessionService {

    @Inject
    SessionRepository repository;

    public List<Session> getSessions() {
        return repository.findAll().list();
    }

    public Session createSession(final NewSessionDTO newSession) {
        final Session session = new Session(newSession.title());
        repository.persist(session);
        return session; // ID is updated
    }

    public Session getSession(final UUID sessionId) {
        return repository.findByIdOptional(sessionId)
                .orElseThrow(NotFoundException::new);
    }

    public Session updateSession(final SessionDTO updatedSession) {
        final Session session = getSession(updatedSession.id());

        session.setTitle(updatedSession.title());
        session.setStartedAt(updatedSession.startedAt());
        session.setResumptionTime(updatedSession.resumptionTime());
        session.setFinished(updatedSession.finished());
        session.setLaps(updatedSession.laps());
        repository.persist(session);

        return session;
    }

    public Session partiallyUpdateSession(final UUID sessionId, final PartialSessionDTO partialSession) {
        final Session session = getSession(sessionId);

        if (partialSession.title() != null) session.setTitle(partialSession.title());
        if (partialSession.startedAt() != null) session.setStartedAt(partialSession.startedAt());
        if (partialSession.resumptionTime() != null) session.setResumptionTime(partialSession.resumptionTime());
        if (partialSession.finished() != null) session.setFinished(partialSession.finished());
        if (partialSession.laps() != null) session.setLaps(partialSession.laps());
        repository.persist(session);

        return session;
    }

    public void deleteSession(final UUID sessionId) {
        final boolean isEntityDeleted = repository.deleteById(sessionId);
        if (!isEntityDeleted) throw new NotFoundException();
    }
}
