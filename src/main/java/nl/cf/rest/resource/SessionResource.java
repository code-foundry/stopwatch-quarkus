package nl.cf.rest.resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import nl.cf.service.domain.Session;
import nl.cf.service.dto.NewSessionDTO;
import nl.cf.service.dto.PartialSessionDTO;
import nl.cf.service.dto.SessionDTO;
import nl.cf.service.SessionService;
import nl.cf.service.dto.SessionLapDTO;
import org.jboss.resteasy.reactive.ResponseStatus;

import java.util.List;
import java.util.UUID;

@Path("/sessions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SessionResource {

    @Inject
    SessionService service;

    @GET
    @ResponseStatus(200)
    public List<Session> getSessions() {
        return service.getSessions();
    }

    @POST
    @ResponseStatus(201)
    public Session createSession(final NewSessionDTO newSession) {
        return service.createSession(newSession);
    }

    @GET
    @Path("/{sessionId}")
    @ResponseStatus(200)
    public Session getSession(final UUID sessionId) {
        return service.getSession(sessionId);
    }

    @PUT
    @Path("/{sessionId}")
    @ResponseStatus(202)
    public Session updateSession(final SessionDTO session, @PathParam("sessionId") final UUID sessionId) {
        if (!sessionId.equals(session.id())) throw new BadRequestException();
        return service.updateSession(session);
    }

    @PATCH
    @Path("/{sessionId}")
    @ResponseStatus(200)
    public Session partiallyUpdateSession(final PartialSessionDTO session, @PathParam("sessionId") final UUID sessionId) {
        return service.partiallyUpdateSession(sessionId, session);
    }

    @DELETE
    @Path("/{sessionId}")
    @ResponseStatus(204)
    public void deleteSession(final UUID sessionId) {
        service.deleteSession(sessionId);
    }

    @PUT
    @Path("/{sessionId}/start")
    public void startSession(final UUID sessionId) {
        throw new NotSupportedException();
    }

    @PUT
    @Path("/{sessionId}/pause")
    public void pauseSession(final UUID sessionId) {
        throw new NotSupportedException();
    }

    @PUT
    @Path("/{sessionId}/finish")
    public void finishSession(final UUID sessionId) {
        throw new NotSupportedException();
    }

    @POST
    @Path("/{sessionId}/laps")
    public void recordSessionLap(final SessionLapDTO lap, @PathParam("sessionId") final UUID sessionId) {
        throw new NotSupportedException();
    }
}
